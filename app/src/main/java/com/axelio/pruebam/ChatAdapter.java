package com.axelio.pruebam;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Axelio on 11/23/16.
 */

public class ChatAdapter extends ArrayAdapter<ChatContact>
{
    public ChatAdapter(Context context, int textViewResourceId)
    {
        super(context, textViewResourceId);
    }

    public ChatAdapter(Context context, int resource, List<ChatContact> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null)
        {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.list_row_chat, null);
        }

        ChatContact item = getItem(position);

        if (item != null)
        {
            TextView textViewName = (TextView) v.findViewById(R.id.textview_name);
            TextView textViewLastDateMessage = (TextView) v.findViewById(R.id.textview_last_message_date);

            if (textViewName != null)
            {
                textViewName.setText(item.name);
            }

            if (textViewLastDateMessage != null)
            {
                textViewLastDateMessage.setText(item.lastMessageDate.toString());
            }

        }

        return v;
    }
}
