package com.axelio.pruebam;

import android.net.Uri;

import java.util.Date;

/**
 * Created by Axelio on 11/23/16.
 */

public class ChatContact
{
    public String name;
    public Date lastMessageDate;
    public String photo_url;
}
